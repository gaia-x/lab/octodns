# Gaia-X octodns

Octodns allows to manage DNS as code. Octodns relies on python to run

This project contains the octodns configuration for Gaia-X domains.
## Requirements
You'll need to add to your environment 3 variables: `OVH_APPLICATION_KEY`, `OVH_APPLICATION_SECRET`, `OVH_CONSUMER_KEY`. The token should be allowed to manage the DNS zone

- python3
## Run octodns
- Install octodns & ovh octodns provider
```shell
pip install -r requirements.txt
```

- Dry-run octodns configuration
```shell
octodns-sync --config-file=ovh.yaml
```
- Apply configuration to DNS
```shell
octodns-sync --config-file=ovh.yaml --do-it
```

## CI
The project contains a CI that will dry-run the configuration when changes reach main branch.
A manual step allows to apply them to the DNS
